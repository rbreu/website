<?php if(!defined('PLX_ROOT')) exit; ?>
<div class="share col sml-12 med-8 lrg-8 sml-centered sml-text-center" style="padding: 0 0; margin-top: 1rem;" >
  
  <br/>Share on:&nbsp;
     
  <a class="social" href="http://www.facebook.com/sharer.php?u=<?php $plxShow->staticUrl() ?>" title="Share on Facebook">
    Facebook
  </a>    
  &nbsp;|&nbsp;                                      
  <a class="social" href="https://twitter.com/share?url=<?php $plxShow->staticUrl() ?>&text=<?php $plxShow->staticTitle() ?>%20%23peppercarrot" title="Retweet on Twitter">
    Twitter
  </a>    
  &nbsp;|&nbsp;                                      
  <a class="social" href="https://plus.google.com/share?url=<?php $plxShow->staticUrl() ?>" title="Share on Google+">
    Google+
  </a>   
  &nbsp;|&nbsp;      
  <a class="social" href="http://reddit.com/submit?url=<?php $plxShow->staticUrl() ?>&title=<?php $plxShow->staticTitle() ?>" title="Share on Reddit">
    Reddit
  </a>    
  
</div>

